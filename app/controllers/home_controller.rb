class HomeController < ApplicationController

	before_action :authenticate_user!
	
	def index
		@data = Coin.find_by_market_cap
	end
end