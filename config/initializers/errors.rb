class CoinError < StandardError
  attr_reader :code , :meta , :error_message

  def initialize(object, options = {},meta=nil)
    msg = ""
    @meta = meta

    if object.class == "".class
      @error_message = I18n.t("error_codes.#{object}", options)
      @code = object
    else
      @error_message = I18n.t("error_codes.#{object.errors.messages.first[1][0]}", options)
      @code = object.errors.messages.first[1][0]
    end

    super(@error_message)
  end
end
