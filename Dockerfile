FROM ruby:alpine
RUN apk add nodejs

RUN apk add --update build-base postgresql-dev tzdata
RUN gem install rails -v '5.2.2'
WORKDIR /app
ADD Gemfile Gemfile.lock /app/
RUN bundle install
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]