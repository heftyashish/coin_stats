# README

This README would normally document whatever steps are necessary to get the
application up and running.

Assuming you have docker, docker-machine and docker-compose installed for your operator system. Below are the steps to build app 

* `docker-compose build`

* `docker-compose up -d`

* check docker ps to list all the containers

* Due to lack of time nginx is being dropped. The reason being nginx container kept changing its port on every build, but Google needed a fixed callback url for a successful signup. We can eliminate the problem on an actual server with a dedicated IP. Nginx docker file, nginx.conf and app docker file are in the repo for your reference, however these are not being used.

* Run `docker-compose run web rails db:create db:migrate`

* Open /etc/hosts file and add `192.168.99.100   coinstats  coinstats.com` as a virtual host, as google only allows public top level domains as redirect URI.

* On your browser, hit `http://coinstats.com:3000/`. The app must be up and running

* In case anything got missed, feel free to email me at `ashish.gandhy@gmail.com`

* ...
