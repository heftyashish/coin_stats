require 'net/http'

module ApiCaller

	LOGGER = Logger.new("log/market_api_#{Rails.env}.log")

  def get_call(url, params, headers = {})
    uri = URI(url)
    response = nil
    LOGGER.info("Sending api call for #{url}")
    LOGGER.info("Params: #{params.to_json}")
    begin
      response = HTTParty.get("#{url}", query: params, headers: headers)
      LOGGER.info("API response code: #{response.code}")
      LOGGER.info("Response: #{response.body}")
      return JSON.parse(response.body).try(:with_indifferent_access)
    rescue => e
      LOGGER.info("Exception in fetching contact: [#{e.class}] #{e.message}")
      LOGGER.debug("Stack trace \n\t" + (e.backtrace or []).join("\n\t"))
    end
  end

end
