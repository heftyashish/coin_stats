class Market::Apis
  extend ApiCaller

  LIMIT = 10
  SORT_BY = {
    cap: 'market_cap'
  }
  HEADERS = {
    'X-CMC_PRO_API_KEY' => Rails.application.secrets.COIN_MARKET_CAP_API_KEY
  }

  def self.currency_list(params)
    raise CoinError.new('Market_101') unless SORT_BY.values.include?(params[:sort_by])
    url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest"

    params = {
      limit: (params[:limit] || LIMIT),
      sort: params[:sort_by],
      sort_dir: 'desc'
    }

  	response = self.get_call(url, params, HEADERS) rescue {}
    data = response[:data] || {}
    return data
  end

end

